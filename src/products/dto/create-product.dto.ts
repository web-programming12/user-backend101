import { MinLength, IsPositive, IsEmpty } from 'class-validator';

export class CreateProductDto {
  @MinLength(8)
  @IsEmpty()
  name: string;

  @IsPositive()
  @IsEmpty()
  price: number;
}
