import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'Espresso', price: 50 },
  { id: 2, name: 'Japanese Milk', price: 45 },
  { id: 3, name: 'Thai Tea', price: 50 },
];
let lastProductId = 4;

@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return products[index];
  }

  remove(id: number) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'Espresso', price: 50 },
      { id: 2, name: 'Japanese Milk', price: 45 },
      { id: 3, name: 'Thai Tea', price: 50 },
    ];
    lastProductId = 4;
    return 'Reset';
  }
}
